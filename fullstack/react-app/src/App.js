import AppNavbar from "./components/AppNavbar"
import CourseView from "./components/CourseView"
import UserView from "./components/UserView"
import BookView from "./components/BookView"
import CreateProduct from "./pages/CreateProduct"
import Books from "./pages/Books"
import Cart from "./pages/Cart"
import Checkout from "./pages/Checkout"
import Home from "./pages/Home"
import Courses from "./pages/Courses"
import Users from "./pages/Users"
import UserTransactions from "./pages/UserTransactions"
import UserPendingTransactions from "./pages/UserPendingTransactions"
import UserShippedOutTransactions from "./pages/UserShippedOutTransactions"
import UserDeliveredTransactions from "./pages/UserDeliveredTransactions"
import UserCancelledTransactions from "./pages/UserCancelledTransactions"
import UserDetails from "./pages/UserDetails"
import AllPendingTransactions from "./pages/AllPendingTransactions"
import AllShippedOutTransactions from "./pages/AllShippedOutTransactions"
import AllDeliveredTransactions from "./pages/AllDeliveredTransactions"
import AllCancelledTransactions from "./pages/AllCancelledTransactions"
import ShippingAddress from "./pages/ShippingAddress"
import ChangePassword1 from "./pages/ChangePassword1"
import UpdateProduct from "./pages/UpdateProduct"
import BookArchives from "./pages/BookArchives"
import {Container} from "react-bootstrap"
import Register from "./pages/Register"
import Login from "./pages/Login"
import Logout from "./pages/Logout"
import Error from "./pages/Error"
import SearchValue from "./pages/SearchValue"
import {useState,useEffect} from 'react';
import {UserProvider} from './UserContext';
import './App.css';
import {BrowserRouter as Router, Route, Routes}
from "react-router-dom"


function App() {
  
  const [user, setUser] = useState({
    id: null,
    isAdmin: null,
  });

  const unsetUser = () => {
    localStorage.clear()
  }


  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      if(typeof data._id !== "undefined") {
        setUser({
          id: data._id,
          username: data.username,
          name: data.name,
          isAdmin: data.isAdmin,
          wishlist: data.wishlist,
          booksPurchased: data.booksPurchased,
          reviews: data.reviews

        })
      } else {
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
  }, [])

  return (
   <>
     <UserProvider value={{user, setUser, unsetUser}}>
       <Router>
         <Routes>
           <Route path="/register" element={<Register excludeNavbar={true} />} />
           <Route path="/login" element={<Login excludeNavbar={true} />}/>
           <Route path="/*" element={<>
             <AppNavbar />
             <Container>
               <Routes>
                 <Route path="/" element={<Home/>}/>
                 <Route path="/courses" element={<Courses/>}/>
                 <Route path="/orders/myCart" element={<Cart/>}/>
                 <Route path="/orders/checkout" element={<Checkout/>}/>
                 <Route path="/books/create" element={<CreateProduct/>}/>
                 <Route path="/books/available1" element={<Books/>}/>
                 <Route path="/books/:bookId" element={<BookView/>}/>
                 <Route path="/books/query" element={<SearchValue/>}/>
                 <Route path="/books/:bookId/update" element={<UpdateProduct/>}/>
                 <Route path="/books/archives" element={<BookArchives/>}/>
                 <Route path="/users/all" element={<Users/>}/>
                 <Route path="/users/:userId/view" element={<UserView/>}/>
                 <Route path="/users/details" element={<UserDetails/>}/>
                 <Route path="/changePassword" element={<ChangePassword1/>}/>
                 <Route path="/users/transactions" element={<UserTransactions/>}/>
                 <Route path="/users/transactions/pending" element={<UserPendingTransactions/>}/>
                 <Route path="/users/transactions/shippedOut" element={<UserShippedOutTransactions/>}/>
                 <Route path="/users/transactions/delivered" element={<UserDeliveredTransactions/>}/>
                 <Route path="/users/transactions/cancelled" element={<UserCancelledTransactions/>}/>
                 <Route path="/transactions/pending" element={<AllPendingTransactions/>}/>
                 <Route path="/transactions/shippedOut" element={<AllShippedOutTransactions/>}/>
                 <Route path="/transactions/delivered" element={<AllDeliveredTransactions/>}/>
                 <Route path="/transactions/cancelled" element={<AllCancelledTransactions/>}/>
                 <Route path="/users/getUserShippingAddress" element={<ShippingAddress/>}/>
                 <Route path="/courses/:courseId" element={<CourseView/>}/>
                 <Route path="/logout" element={<Logout/>}/>
                 <Route path="*" element={<Error/>} />
               </Routes>
             </Container>
           </>} />
         </Routes>
       </Router>
     </UserProvider>
   </>


  );
}

export default App;
