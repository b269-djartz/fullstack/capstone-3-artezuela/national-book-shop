import {useState,useEffect,useContext} from 'react';
import {Link} from "react-router-dom"
import UserContext from '../UserContext'
import Swal from 'sweetalert2';

import { Button, Row, Col, Card } from 'react-bootstrap';



export default function BookCard({book,handleRefresh}) {

    const {ratings,starRating,title,author,genre,stock,description,price,_id} = book;


    const {user} = useContext(UserContext)

    const [isBookInWishlist, setIsBookInWishlist] = useState(false);
    
    useEffect(() => {
    
    fetch(`${process.env.REACT_APP_API_URL}/users/checkUsersWishlist`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },body: JSON.stringify({
        bookId: book._id
      })
    })
    .then(res => res.json())
    .then(data => {
      if (data=== true) {
        
        setIsBookInWishlist(true)

      } else {
      console.log(data);
      }
      
    })
  }, [])

   function archiveProduct () {

      fetch(`${process.env.REACT_APP_API_URL}/books/${_id}/archive`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },body: JSON.stringify({
        bookId: book._id
      })
    })
    .then(res => res.json())
    .then(data => {
      if (data=== true) {
        
       Swal.fire({
            title: 'Book Archived!',
            icon: 'success',
            text: 'The book will not be available for selling',
          });

      } else {
      console.log(data);
      }
      
    })

   }


  return (
  <Col className="mb-3 mt-3">
    <Card className="book-card p-0">
      <Card.Img  variant="top" src={`https://picsum.photos/200?${title}-${author}`} />
      <Card.Body className="text-center">
        <Card.Title className="_card_title" as={Link} to={`/books/${_id}`} >
          <h4 >{title}</h4>
        </Card.Title> 
        <Card.Text>by: {author}</Card.Text>
        <Card.Text>Price: <b>{price}</b></Card.Text>
        <Card.Text>ratings: <b>{ratings}</b></Card.Text>
        <Card.Text className="_cardX star-rating" data-rating={starRating}></Card.Text>
        {(() => {
          if (user.id && isBookInWishlist) {
            return (
              <>
                <Card.Subtitle>This Book is in your wishlist</Card.Subtitle>
              </>
            );
          }
        })()}
        { user.isAdmin ? 

          <>
          <Button as={Link} to={`/books/${_id}/update`}> Edit book </Button>
          <Button variant="danger" onClick={() => {
                                                    archiveProduct();
                                                    handleRefresh();
                                                  }}> Archive </Button> 
          </>
          :
          <span></span>

        }
      </Card.Body>
    </Card>
  </Col>
);

}

