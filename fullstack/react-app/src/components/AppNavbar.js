import { Nav, Navbar, NavDropdown, FormControl, Form, Button, Container, InputGroup, Col, Row, Image,Dropdown,DropdownButton  } from 'react-bootstrap';
import logo from "../images/mylogo.png"
import cartlogo from "../images/cartlogo.png"
import { useContext, useState , useEffect} from 'react';
import UserContext from '../UserContext';
import SearchValueCard from "../components/SearchValueCard";
import { useParams, useNavigate, Link } from 'react-router-dom';

export default function AppNavbar(props) {
  const { user } = useContext(UserContext);
  
  const navigate = useNavigate('');

  const [searchValues, setSearchValues] = useState([]);
  const [refresh, setRefresh] = useState([]);
  const [showValue, setShowValue] = useState(false);

const [sortOption, setSortOption] = useState(null);
  const [filterGenre, setFilterGenre] = useState(null);

  const { excludeNavbar } = props;

  if (excludeNavbar) {
    return null;
  }
  
 


  function searchBox(searchInput) {
  fetch(`${process.env.REACT_APP_API_URL}/books/query`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                query: searchInput
            })
        })
    .then(response => response.json())
    .then(data => {
      if(data.error) {
        return alert("nothing in here")
      }
      console.log(data)
      setSearchValues(data)
       setShowValue(true)
  
  })
}


  const sortedBooks = [...searchValues]
    .filter((book) => (filterGenre ? book.genre === filterGenre : true))
    .sort((a, b) => {
      if (!sortOption) return 0;
      if (sortOption === 'highest-price') return b.price - a.price;
      if (sortOption === 'lowest-price') return a.price - b.price;
      if (sortOption === 'highest-sold') return b.sold - a.sold;
      if (sortOption === 'lowest-sold') return a.sold - b.sold;
      if (sortOption === 'highest-star') return b.rating - a.rating;
      if (sortOption === 'lowest-star') return a.rating - b.rating;
      if (sortOption === 'highest-rating') return b.ratingCount - a.ratingCount;
      if (sortOption === 'lowest-rating') return a.ratingCount - b.ratingCount;
      if (sortOption === 'highest-stock') return b.stock - a.stock;
      if (sortOption === 'lowest-stock') return a.stock - b.stock;
      return 0;
    });

  const genres = ['All Genres', 'Fantasy', 'Self-help', 'Romance'];

 function handleChange  () {
    setSearchValues([])
    setShowValue(false)
  }

  return (
    <>
    <Container>
    <Navbar expand="lg" variant="light" bg="light" className="fixed-top">
      <Container>
        <Row className="w-100">
          <Col xs={12} className="pb-2 pb-md-0">
            <Navbar>
            <Container className="_nav-seller">
            { user.isAdmin  ?  


              <>
              <Nav.Link href="/books/create" className="border-end pe-3">Create book</Nav.Link>
              <Nav.Link href="/books/archives" className="border-end ps-3 pe-3">Book Archives</Nav.Link>
              <Nav.Link href="/transactions/pending" className="border-end pe-3 ps-3">Transactions</Nav.Link>
              <Nav.Link href="/users/all" className="ps-3"> Users</Nav.Link>
              </>
              


              : user.id ? 

              <>
              <Nav.Link href="/" className="border-end pe-3">Seller Center</Nav.Link>
              <Nav.Link href="/" className="ps-3">Start selling</Nav.Link>
              </>
              


              :

              <>
              <Nav.Link href="/register" className="border-end pe-3">Register</Nav.Link>
              <Nav.Link href="/login" className="ps-3">Login</Nav.Link>
              </>


          }
         
            </Container>
            <Container className="_nav-username">
            { user.id ?
            
            
            <NavDropdown title={user.username} id="navbarScrollingDropdown">
                 <NavDropdown.Item href="/users/details">My Account</NavDropdown.Item>
                 <NavDropdown.Item href="/users/transactions/pending">My Purchase</NavDropdown.Item>
                 <NavDropdown.Divider />
                 <NavDropdown.Item href="/logout">Logout</NavDropdown.Item>
             </NavDropdown>


             :


             <>

             <Nav.Link href="/" className="ps-3">Hi! Guest!</Nav.Link>
             </>


            }

              
              </Container>
            </Navbar>
          </Col>
          <Col md={2} xs={0} className="d-flex align-items-center justify-content-center d-none d-md-flex">
            <Navbar.Brand className=" d-md-inline" href="/">
              <Image className="_logo d-inline" src={logo} alt="National Book Shop" fluid/>
            </Navbar.Brand>
          </Col>
          <Col md={8} xs={10} className="d-flex align-items-center justify-content-center">
            <Form className="d-flex" onSubmit={event => {
              event.preventDefault();
              searchBox(event.target.elements.searchBar.value);
              navigate("/books/query")
            }}>
              <InputGroup>
                <FormControl id="search-bar"name="searchBar" type="search" placeholder="Search for an item" aria-label="Search" />
                <Button type="submit" variant="outline-danger" id="btn-search" style={{ backgroundColor: "red" }}>&#128269;</Button>
              </InputGroup>
            </Form>

          </Col>
          <Col md={2} xs={2} className="d-flex align-items-center justify-content-center">
            <Navbar.Brand className="cart-wrapper" href="/orders/myCart">
              <Image className="_cartlogo d-inline" src={cartlogo} alt="National Book Shop" fluid />
              <span className="cart-count">100</span>
            </Navbar.Brand>
          </Col>
        </Row>
      </Container>
    </Navbar>
    </Container>
    <Container>
    <div className="_theMargin">
    {showValue ? 

    <Container>
      <Row>
        <div className="d-flex align-items-center mt-3 justify-content-start">
        <div className="me-5">
          <DropdownButton className="_sort_button" title="Sort By" className="mr-2">
            <Dropdown.Item onClick={() => setSortOption('highest-price')}>
              Highest Price
            </Dropdown.Item>
            <Dropdown.Item onClick={() => setSortOption('lowest-price')}>
              Lowest Price
            </Dropdown.Item>
            <Dropdown.Item onClick={() => setSortOption('highest-sold')}>
              Highest Sold
            </Dropdown.Item>
            <Dropdown.Item onClick={() => setSortOption('lowest-sold')}>
              Lowest Sold
            </Dropdown.Item>
            <Dropdown.Item onClick={() => setSortOption('highest-star')}>
              Highest Star
            </Dropdown.Item>
            <Dropdown.Item onClick={() => setSortOption('lowest-star')}>
              Lowest Star
            </Dropdown.Item>
            <Dropdown.Item onClick={() => setSortOption('highest-rating')}>
              Highest Rating
            </Dropdown.Item>
            <Dropdown.Item onClick={() => setSortOption('lowest-rating')}>
              Lowest Rating
            </Dropdown.Item>
            <Dropdown.Item onClick={() => setSortOption('highest-stock')}>
              Highest Stock
            </Dropdown.Item>
            <Dropdown.Item onClick={() => setSortOption('lowest-stock')}>
              Lowest Stock
            </Dropdown.Item>
          </DropdownButton>
         </div>
          {genres.map((genre) => (
            <Button
              key={genre}
              onClick={() => setFilterGenre(genre === 'All Genres' ? null : genre)}
              variant={filterGenre === genre ? 'danger' : 'secondary'}
              className="_genre_buttons">{genre}
      </Button>
        ))}
    </div>
    </Row>
    <Row>
      {sortedBooks.map((book) => (
    <Col sm={6} md={4} lg={3} key={book.id} className="book-card-col">
    <div className="card-container">
    <SearchValueCard searchValue={book} handleChange={handleChange} />
    </div>
    </Col>
    ))}
    </Row>
    </Container>


    :
    <div></div>

  }
    
    </div>
    </Container>
    </>
   
  );
}
