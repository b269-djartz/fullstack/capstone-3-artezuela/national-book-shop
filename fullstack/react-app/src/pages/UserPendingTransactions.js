/*import coursesData from "../data/coursesData"*/
import {useState, useEffect} from 'react';
import CartCard from "../components/CartCard";
import { Button } from 'react-bootstrap';
import {Link} from "react-router-dom"
import Swal from 'sweetalert2';
import UserPendingTransactionsCard from "../components/UserPendingTransactionsCard";
import { NavLink, useNavigate } from 'react-router-dom';

export default function UserPendingTransactions() {

	const [userPendingTransactions, setUserPendingTransactions] = useState([]);
	const navigate = useNavigate("");
	const [refresh, setRefresh] = useState(false);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/users/transactions/pending`, {
		  method: 'POST',
		  headers: {
		    'Content-Type': 'application/json',
		    Authorization: `Bearer ${localStorage.getItem('token')}`,
		  }
		})
		.then(res => res.json())
		.then(data => {
			if (data.length > 0) {
				setUserPendingTransactions(data.map(userPendingTransaction => {
				return(
					<>
					<UserPendingTransactionsCard
					        key={userPendingTransaction._id}
					        userPendingTransaction={userPendingTransaction}
					        cancelOrder={cancelOrder}
					      />


					</>

				)
			}))
			} else if (data.noTransactions) {
				return Swal.fire({
            	title: 'No Pending Transactions!',
            	icon: 'warning',
            	text: 'Purchase a product',
          });
			} else {
				return console.log(data);
			}
			
		})
	}, [])

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/users/transactions/pending`, {
		  method: 'POST',
		  headers: {
		    'Content-Type': 'application/json',
		    Authorization: `Bearer ${localStorage.getItem('token')}`,
		  }
		})
		.then(res => res.json())
		.then(data => {
			if (data.length > 0) {
				setUserPendingTransactions(data.map(userPendingTransaction => {
				return(
					<>
					<UserPendingTransactionsCard
					        key={userPendingTransaction._id}
					        userPendingTransaction={userPendingTransaction}
					        cancelOrder={cancelOrder}
					      />


					</>

				)
			}))
			} else if (data.noTransactions) {
				return setUserPendingTransactions([])
			} else {
				return console.log(data);
			}
			
		})
	}, [refresh])


	function cancelOrder(transactionId) {
    fetch(`${process.env.REACT_APP_API_URL}/users/cancelTransactions`, {
      method: 'POST',
      headers: {
              'Content-Type': 'application/json',
              Authorization: `Bearer ${localStorage.getItem('token')}`,
            },
            body: JSON.stringify({
              transactionId: transactionId,
            }),
          })
    .then(res => res.json())
    .then(data => {
      if(data===true) {
        alert("cancelled success");
        setRefresh(!refresh);
        

      }

    })
  }


	return (
	  <>
	    <div style={{ display: 'flex', justifyContent: 'center', gap: '20px', marginTop: '20px' }}>
	      <NavLink to="/users/transactions">All Transactions</NavLink>
	      <NavLink to="/users/transactions/pending">Pending</NavLink>
	      <NavLink to="/users/transactions/shippedOut">Shipped Out</NavLink>
	      <NavLink to="/users/transactions/delivered">Delivered</NavLink>
	      <NavLink to="/users/transactions/cancelled">Cancelled</NavLink>
	    </div>
	    {userPendingTransactions.length > 0 ? userPendingTransactions : <p>No Pending Transactions</p>}
	  </>
	);


}

