import {useState, useEffect, useContext} from 'react';
import {Form, Button,Container,Navbar,Nav, NavDropdown,Image} from 'react-bootstrap';
import Swal from 'sweetalert2';
// import {useNavigate} from 'react-router-dom';
import {Navigate,useNavigate} from 'react-router-dom';
import mylogo from "../images/mylogo.png"

import UserContext from '../UserContext';

export default function Login(){

    // "useContext" hook that allows you to access and update the shared state from any component in your application.
    // "UserContext" - global state that can be shared and updated throughout the application
    const {user, setUser} = useContext(UserContext);
    const navigate = useNavigate()

    // to store values of the input fields
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    // to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false)

    // hook returns a function that lets you navigate to components
    // const navigate = useNavigate()

    function authenticate(e){
        e.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            // We will receive either a token or an error response.
            console.log(data);

            // If no user information is found, the "access" property will not be available and will return undefined
            // Using the typeof operator will return a string of the data type of the variable/expression it preceeds which is why the value being compared is in a string data type
            if(typeof data.access !== "undefined") {
                // The JWT will be used to retrieve user information across the the whole frontend application and storing it in the localStorage will allow ease of access to the user's information
                localStorage.setItem('token', data.access);
                retrieveUserDetails(data.access);

                Swal.fire({
                    title: "Login Successful",
                    icon: "success",
                    text: "Welcome to Zuitt"

                })
                setEmail('')
                setPassword('')
                navigate("/")
            } else if (data.passwordIncorrect) {
                Swal.fire({
                    title: "Password Incorrect",
                    icon: "error",
                    text: "Please try again"
                })
                setPassword('')

            } else if(data.emailNotFound) {
                Swal.fire({
                    title: "Email doesn't exists",
                    icon: "error",
                    text: "Please try again"
                })
                setEmail('')
            }
    });

        

        
};
    const retrieveUserDetails = (token) => {
            // The token will be sent as part of the request's header information
            // We put "Bearer" in front of the token to follow implementation standards for JWTs
            fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })
            .then(res => res.json())
            .then(data => {
                console.log(data);
                // Global user state for validation accross the whole app
                // Changes the global "user" state to store the "id" and the "isAdmin" property of the user which will be used for validation across the whole application
                setUser({
                    id: data._id,
                    isAdmin: data.isAdmin,
                    username: data.username,
                    wishlist: data.wishlist,
                    booksPurchased: data.booksPurchased,
                    reviews: data.reviews
                })
            })
        };

    useEffect(() => {
        if((email !== '' && password !== '')){
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [email, password])

    return(
        (user.id !== null) ?
        <Navigate to="/" />
        :
        <>
         <Navbar className="fixed-top" bg="light" expand="lg">
          <Container>
            <Navbar.Brand   href="/">
                  <Image className="_logo1" src={mylogo} alt="National Book Shop" fluid/>
            </Navbar.Brand>
          </Container>
        </Navbar>
        <Container>
            <Form onSubmit={e => authenticate(e)} className="_login_form w-50 mt-5 mx-auto pt-5">
                <h1 className="text-center">Login</h1>
                <Form.Group controlId="userEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control 
                        type="email" 
                        placeholder="Enter email"
                        value={email}
                        onChange={e => setEmail(e.target.value)}
                        required
                    />
                </Form.Group>

                <Form.Group controlId="password">
                    <Form.Label>Password</Form.Label>
                    <Form.Control 
                        type="password" 
                        placeholder="Password"
                        value={password}
                        onChange={e => setPassword(e.target.value)}
                        required
                    />
                </Form.Group>

                {   isActive ?
                    <Button className="mt-3" variant="danger" type="submit" id="submitBtn">
                        Submit
                    </Button>
                    :
                    <Button className="mt-3" variant="danger" type="submit" id="submitBtn" disabled>
                        Submit
                    </Button>
                }
            </Form>
        </Container>
        </>
        
    )
};
