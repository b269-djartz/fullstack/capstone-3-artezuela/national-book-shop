/*import coursesData from "../data/coursesData"*/
import {useState, useEffect,useContext} from 'react';
import UserContext from '../UserContext';
import { Button } from 'react-bootstrap';
import {Link} from "react-router-dom"
import Swal from 'sweetalert2';
import AllDeliveredTransactionsCard from "../components/AllDeliveredTransactionsCard";
import { NavLink, useNavigate } from 'react-router-dom';

export default function AllDeliveredTransactions() {

	const [allDeliveredTransactions, setAllDeliveredTransactions] = useState([]);
	const navigate = useNavigate("");
	const [refresh, setRefresh] = useState(false);
	const {user} = useContext(UserContext)


	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/transactions/delivered`, {
		  method: 'POST',
		  headers: {
		    'Content-Type': 'application/json',
		    Authorization: `Bearer ${localStorage.getItem('token')}`,
		  }
		})
		.then(res => res.json())
		.then(data => {
			if (data.length > 0) {
				console.log(data)
				setAllDeliveredTransactions(data.map(allDeliveredTransaction => {
				return(
					<>
					<AllDeliveredTransactionsCard
					        key={allDeliveredTransaction._id}
					        allDeliveredTransaction={allDeliveredTransaction}
					      />


					</>

				)
			}))
			} else if (data.noTransactions) {
				return Swal.fire({
            	title: 'No Delivered Transactions!',
            	icon: 'warning'
          });
			} else {
				return console.log(data);
			}
			
		})
	}, [])


	return (
	  <>
	    <div style={{ display: 'flex', justifyContent: 'center', gap: '20px', marginTop: '20px' }}>
	      <NavLink to="/transactions/pending">Pending</NavLink>
	      <NavLink to="/transactions/shippedOut">Shipped Out</NavLink>
	      <NavLink to="/transactions/delivered">Delivered</NavLink>
	      <NavLink to="/transactions/cancelled">Cancelled</NavLink>
	    </div>
	    {allDeliveredTransactions.length > 0 ? allDeliveredTransactions : <p>No Delivered Transactions</p>}
	  </>
	);


}

