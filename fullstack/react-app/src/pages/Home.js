import Banner from "../components/Banner"
import Books from "../pages/Books"
import { Container, Card, Button, Row, Col, Form } from 'react-bootstrap';



export default function Home() {
	return (
		<>
			<Banner/>
			<Container>
			<Books/>
			</Container>
		</>
	)
}