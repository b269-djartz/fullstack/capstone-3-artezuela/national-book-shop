import {useState, useEffect,useContext} from 'react';
import { Form, Button } from 'react-bootstrap';
import {Navigate,useNavigate,Link} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2';

export default function Register() {
   
    const {user, setUser} = useContext(UserContext)

    // to store and manage value of the input fields
    const [title, setTitle] = useState("");
    const [genre, setGenre] = useState("");
    const [author, setAuthor] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState(0);
    const [stock, setStock] = useState(0);
    const [tags, setTags] = useState([]);

    // to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false);

    const navigate = useNavigate('')

    

    useEffect(() => {
      if (title && genre && author && description && price) {
        setIsActive(true);
      } else {
        setIsActive(false);
      }
    }, [title, genre, author, description, price]);




function addBook(e) {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/books/create`, {
        method: 'POST',
        headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
        body: JSON.stringify({
            title: title,
            genre: genre,
            author: author,
            description: description,
            price: price,
            stock: stock,
            tags: tags,
        })
    })
    .then(res => res.json())
    .then(data => {
        console.log(data)
        if(data.creationSuccess) {
            setTitle("")
            setGenre("")
            setAuthor("");
            setPrice("");
            setStock("");
            setTags("")
          
            Swal.fire({
                    title: "Book successfully added",
                    icon: "success",
                    text: `Add more!`
                })
        } else if (data.userNotAdmin) {
            alert("User is not admin")
        } else {
          navigate("/*")
        }
    });
}


    return (
         (user.isAdmin === false)? 
        <Navigate to="/courses"/>
         :
    <Form className="w-50 mx-auto"onSubmit={(e) => addBook(e)}>
      
      <Form.Group controlId="title">
        <Form.Label>Title</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter book title"
          value={title}
          onChange={(e) => setTitle(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group controlId="genre">
        <Form.Label>Genre</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter book genre"
          value={genre}
          onChange={(e) => setGenre(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group controlId="author">
        <Form.Label> Author</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter book author"
          value={author}
          onChange={(e) => setAuthor(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group controlId="description">
        <Form.Label>Description</Form.Label>
        <Form.Control
          as="textarea"
          rows={3}
          placeholder="Enter description"
          value={description}
          onChange={(e) => setDescription(e.target.value)}
          style={{ height: "90px" }}
          required
        />
      </Form.Group>

      <Form.Group controlId="price">
        <Form.Label>Price</Form.Label>
        <Form.Control
          type="number"
          placeholder="Enter price"
          value={price}
          onChange={(e) => setPrice(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group controlId="stock">
        <Form.Label>Stock</Form.Label>
        <Form.Control
          type="number"
          placeholder="Enter stock"
          value={stock}
          onChange={(e) => setStock(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group controlId="tags">
        <Form.Label>Tags</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter tags separated by comma"
          value={tags}
          onChange={(e) => setTags(e.target.value.split(','))}
          required
        />
      </Form.Group>

      

        <Button className="mt-3 mb-5" variant="primary" type="submit" disabled={!isActive}>
          Create
        </Button>
      </Form>
      );
  


}
