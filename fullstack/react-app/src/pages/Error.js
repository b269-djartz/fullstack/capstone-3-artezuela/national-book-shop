import Banner from "../components/Banner"
import { Link } from "react-router-dom"

export default function Error() {
  return (
    <>
      <Banner
      />
      <h1 className="text-center">404 Not Found</h1>
    </>
  );
}
