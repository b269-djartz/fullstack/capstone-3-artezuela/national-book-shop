import { Button, Row, Col, Form, FormControl } from "react-bootstrap";
import { useState, useEffect } from "react";
import {useNavigate, Link} from "react-router-dom"
import Swal from 'sweetalert2';
// import ShippingAddressesCard from "../components/ShippingAddressesCard"


export default function CheckOut() {
  const navigate = useNavigate('');
  const [shippingFee, setShippingFee] = useState(0);
  const [total, setTotal] = useState(0);
  const [voucherDiscount, setVoucherDiscount] = useState(0);
  const [subtotal, setSubtotal] = useState(0);
  const [voucherCode, setVoucherCode] = useState("");
  const [region, setRegion] = useState("");
  const [province, setProvince] = useState("");
  const [municipality, setMunicipality] = useState("");
  const [barangay, setBarangay] = useState("");
  const [completeAddress, setCompleteAddress] = useState("");
  const [isEditable, setIsEditable] = useState(false);

  const [paymentMethod, setPaymentMethod] = useState("");
  const [paymentAmount, setPaymentAmount] = useState(0);
  const [refresh,setRefresh] = useState(false)


  useEffect(() => {
  const fetchData = async () => {
    try {
      const locationResponse = await fetch(
        `${process.env.REACT_APP_API_URL}/users/getUserShippingAddress2`,
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Authorization: "Bearer " + localStorage.getItem("token"),
          }
        }
      );
      const locationData = await locationResponse.json();
      console.log(locationData)
      if (typeof locationData === "object") {
        setRegion(locationData.region)
        setProvince(locationData.province)
        setMunicipality(locationData.municipality)
        setBarangay(locationData.barangay)
        setCompleteAddress(locationData.completeAddress)

        const totalAmountResponse = await fetch(
          `${process.env.REACT_APP_API_URL}/orders/totalAmount`,
          {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
              Authorization: "Bearer " + localStorage.getItem("token"),
            },
            body: JSON.stringify({ voucher: voucherCode,
              region: locationData.region,
              province: locationData.province,
              municipality: locationData.municipality,
              barangay: locationData.barangay,
              completeAddress: locationData.completeAddress }),
          }
        );
        const totalAmountData = await totalAmountResponse.json();
        console.log(totalAmountData)
        if (typeof totalAmountData === "object") {
          setShippingFee(totalAmountData.shippingFee);
          setVoucherDiscount(totalAmountData.voucherDiscount);
          setSubtotal(totalAmountData.subtotal);
          setTotal(totalAmountData.totalAmount);
        } else {
          console.log("Error fetching total amount")
        }
      } else {
        console.log("Error fetching user location")
      }
    } catch (error) {
      console.log(error)
    }
  };

  fetchData();
}, [voucherCode]);




 useEffect(() => {
    if (voucherDiscount > 0) {
      window.alert('Congratulations!');
    }
  }, [voucherDiscount]);




//   useEffect(() => {
//   const fetchTotalAmount =  () => {
//     fetch(
//       `${process.env.REACT_APP_API_URL}/orders/totalAmount`,
//       {
//         method: "POST",
//         headers: {
//           "Content-Type": "application/json",
//           Authorization: "Bearer " + localStorage.getItem("token"),
//         },
//         body: JSON.stringify({ 
//           voucher: voucherCode,
//           region: region,
//           province: province,
//           municipality: municipality,
//           barangay: barangay,
//           completeAddress: completeAddress 
//         })
//       }
//     )
//     .then(response => response.json())
//     .then(data => {
//       console.log(data)
//       if (typeof data === "object") {
//         setShippingFee(data.shippingFee);
//         setVoucherDiscount(data.voucherDiscount);
//         setSubtotal(data.subtotal);
//         setTotal(data.totalAmount);
//         setRegion(data.region)
//         setProvince(data.province)
//         setMunicipality(data.municipality)
//         setBarangay(data.barangay)
//         setCompleteAddress(data.completeAddress)
//       } else {
//         console.log("error")
//       }
//     })
//   }; 

//   fetchTotalAmount();
// }, [voucherCode]);


 useEffect(() => {
    if (voucherDiscount > 0) {
      window.alert('Congratulations!');
    }
  }, [voucherDiscount]);


  const handleEditVoucherCode = () => {
    setIsEditable(true);
    setRefresh(!refresh)
  }

  const handleSaveVoucherCode = () => {
    setIsEditable(false);
    setRefresh(!refresh)
  }

    const handlePaymentMethodChange = (e) => {
    setPaymentMethod(e.target.value);
  };

  const handlePaymentAmountChange = (e) => {
    setPaymentAmount(e.target.value);
  };

  const checkOut = () => {

    fetch(`${process.env.REACT_APP_API_URL}/orders/checkout`, {
        method: 'POST',
        headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
        body: JSON.stringify({
            voucher: voucherDiscount,
            paymentMethod: paymentMethod,
            paymentAmount: paymentAmount,
            region: region,
            province: province,
            municipality: municipality,
            barangay: barangay,
            completeAddress: completeAddress
        })
    })
    .then(res => res.json())
    .then(data => {
      console.log(data)
      if(data.allTrue) {
          console.log(data)
         Swal.fire({
                    title: "Success!",
                    icon: "success",
                    text: `Checkout Successful`
                })
         navigate("/")
      } else if (data.invalidPayment) {
        Swal.fire({
                    title: "Error!",
                    icon: "error",
                    text: `You don't have enough cash`
                })
      } else if (data.invalidPaymentMethod) {
        Swal.fire({
                    title: "Error!",
                    icon: "error",
                    text: `Select a payment method`
                })
      } else {
        console.log("error")
      }
    })
    // your checkout logic here
  };

  const handleRefresh = () => {
    setRefresh(!refresh)
  }


  return (


      <Row>
        <Col className="p-5">
          <Form>
            <Form.Group controlId="formVoucherCode">
              <Form.Label>Voucher Code</Form.Label>
              <div className="d-flex">
                <FormControl className="w-50"
                  type="text"
                  placeholder="Enter voucher code"
                  value={voucherCode}
                  onChange={(e) => setVoucherCode(e.target.value)}
                  disabled={!isEditable}
                />
                {isEditable ? (
                  <Button variant="secondary" onClick={handleSaveVoucherCode} className="ms-3">
                    Save
                  </Button>
                ) : (
                  <Button variant="primary" onClick={handleEditVoucherCode} className="ms-3">
                    Edit
                  </Button>
                )}
              </div>
            </Form.Group>
            <Form.Group>
              <Form.Label>{completeAddress},{barangay},{municipality},{province},{region}</Form.Label>
            </Form.Group>
            <Button as={Link} to="/users/getUserShippingAddress">
              Edit Shipping Address
            </Button>
            <Form.Group>
              <Form.Label>Shipping Fee:</Form.Label>
              <Form.Label>{shippingFee}</Form.Label>
            </Form.Group>
            <Form.Group>
              <Form.Label>Voucher Discount:</Form.Label>
              <Form.Label>{voucherDiscount}</Form.Label>
            </Form.Group>
            <Form.Group>
              <Form.Label>Subtotal:</Form.Label>
              <Form.Label>{subtotal}</Form.Label>
            </Form.Group>
            <Form.Group>
              <Form.Label>Total:</Form.Label>
              <Form.Label>{total}</Form.Label>
            </Form.Group>
            <Form.Group controlId="formPaymentMethod">
              <Form.Label>Payment Method:</Form.Label>
              <div className="d-flex">
                <Form.Check
                  type="radio"
                  label="Cash on Delivery"
                  name="paymentMethod"
                  id="cash-on-delivery"
                  checked={paymentMethod === "cashOnDelivery"}
                  onChange={() => {
                    setPaymentMethod("cashOnDelivery");
                    setPaymentAmount(0);
                  }}
                />
                <Form.Check
                  type="radio"
                  label="E-wallet"
                  name="paymentMethod"
                  id="e-wallet"
                  checked={paymentMethod === 'E-wallet'}
                  onChange={() => setPaymentMethod('E-wallet')}
                  className="ms-3"
                />
                {paymentMethod === 'E-wallet' && (
                  <div className="ms-3">
                    <Form.Control
                      type="number"
                      placeholder="Enter e-wallet amount"
                      value={paymentAmount}
                      onChange={(e) => setPaymentAmount(e.target.value)}
                      disabled={paymentMethod !== 'E-wallet'}
                    />
                  </div>
                )}
              </div>
            </Form.Group>
            <Button onClick={checkOut} variant="primary">
              checkout
            </Button>
          </Form>
        </Col>
      </Row>
    );




}
