const express = require("express");
const router = express.Router();
const auth = require("../auth");
const orderController = require("../controllers/orderController");

//<<-------------ADD TO CART---------------->>\\
router.post("/addToCart", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		userId : auth.decode(req.headers.authorization).id,
		reqBody: req.body
	};
	orderController.addToCart(data).then(resultFromController => res.send(resultFromController));
});

//<<-------------VIEW MY CART--------------->>\\
router.post("/myCart",auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
orderController.myCart(userData.id).then(resultFromController => res.send(resultFromController));
});

//<<-------------VIEW MY CART--------------->>\\
router.put("/orders/:orderId/change",auth.verify, (req, res) => {
	const data = {
		userId :  auth.decode(req.headers.authorization).id,
		reqBody: req.body
	};
orderController.changeOrder(data).then(resultFromController => res.send(resultFromController));
});

//<<-------------CHANGE QUANTITY--------------->>\\
router.patch("/:orderId/changeQuantity", auth.verify, (req, res) => {
	orderController.changeQuantity(req.params, req.body ).then(resultFromController => res.send(resultFromController));
});

//<<-------------REMOVE PRODUCT FROM CART--------------->>\\
router.delete("/:orderId/delete", auth.verify, (req, res) => {
	orderController.removeProduct(req.params).then(resultFromController => res.send(resultFromController));
});

//<<-------------TOTAL AMOUNT IN CART--------------->>\\
router.post("/totalAmount",auth.verify, (req, res) => {
	const data = {
		userData : auth.decode(req.headers.authorization).id ,
		reqBody : req.body
	}
	orderController.totalAmount(data).then(resultFromController => res.send(resultFromController));
});

//<<-------------CHECKOUT--------------->>\\
router.post("/checkout", auth.verify, (req, res) => {
	const data = {
		userData : auth.decode(req.headers.authorization).id,
		reqBody : req.body
	}
	orderController.checkout(data).then(resultFromController => res.send(resultFromController));
});



module.exports = router;