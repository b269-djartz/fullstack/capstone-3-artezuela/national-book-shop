const express = require("express");
const router = express.Router();
const bookController = require("../controllers/bookController");
const userController = require("../controllers/userController");
const auth = require("../auth");

//<<-------------CREATE PRODUCT (ADMIN)---------------->>\\
router.post("/create", auth.verify, (req, res) => {
	const data = {
		book: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	bookController.addBook(data).then(resultFromController => res.send(resultFromController));
});

//<<-------------SUGGEST AVAILABLE PRODUCTS---------------->>\\
router.get("/suggest" ,auth.verify, (req, res) => {

	const data = {
		userId: auth.decode(req.headers.authorization).id,
		sort: req.body.sort,
		reqBody: req.body
	}
	bookController.suggestAvaialableBooks(data).then(resultFromController => res.send(resultFromController));
});

router.get("/available1" , (req, res) => {
	bookController.getAllAvailableBooks1().then(resultFromController => res.send(resultFromController));
});

router.get("/archives" , (req, res) => {
	bookController.getBookArchives().then(resultFromController => res.send(resultFromController));
});


//<<-------------RETRIEVE ALL AVAILABLE PRODUCTS---------------->>\\
router.get("/available" ,auth.verify, (req, res) => {

	const data = {
		userId: auth.decode(req.headers.authorization).id,
		sort: req.body.sort
	}
	bookController.getAllAvailableBooks(data).then(resultFromController => res.send(resultFromController));
});

//<<-------------RETRIEVE ALL AVAILABLE ROMANCE PRODUCTS---------------->>\\
router.get("/romanceAvailable" ,auth.verify, (req, res) => {
	const data = {
		userId: auth.decode(req.headers.authorization).id,
		sort: req.body.sort
	}
	bookController.getRomanceAvaialableBooks(data).then(resultFromController => res.send(resultFromController));
});

//<<-------------RETRIEVE ALL AVAILABLE SELF-HELP PRODUCTS---------------->>\\
router.get("/self-helpAvailable" ,auth.verify, (req, res) => {
	const data = {
		userId: auth.decode(req.headers.authorization).id,
		sort: req.body.sort
	}
	bookController.getSelfHelpAvaialableBooks(data).then(resultFromController => res.send(resultFromController));
});

//<<-------------RETRIEVE ALL AVAILABLE FANTASY PRODUCTS---------------->>\\
router.get("/fantasyAvailable" ,auth.verify, (req, res) => {
	const data = {
		userId: auth.decode(req.headers.authorization).id,
		sort: req.body.sort
	}
	bookController.getFantasyAvaialableBooks(data).then(resultFromController => res.send(resultFromController));
});

//<<-------------RETRIEVE SINGLE PRODUCT---------------->>\\
router.get("/:bookId", (req, res) => {
	const data = {
		reqParams : req.params
	}
	bookController.getBook(data).then(resultFromController => res.send(resultFromController));
});

//<<-------------UPDATE SINGLE PRODUCT (ADMIN)---------------->>\\
router.put("/:bookId/update",auth.verify, (req,res) => {
	const data = {
		reqBody: req.body,
		reqParams: req.params,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	bookController.updateBook(data).then(resultFromController => res.send(resultFromController));
});

//<<-------------ARCHIVE PRODUCT (ADMIN)---------------->>\\
router.patch("/:bookId/archive", auth.verify, (req, res) => {
	const data = {	
		reqParams: req.params,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	bookController.archiveBook(data).then(resultFromController => res.send(resultFromController));
});

//<<-------------ADD REVIEW ---------------->>\\
router.post("/:bookId/addReview", auth.verify, (req, res) => {
    const data = {
        reqParams: req.params,
        userId: auth.decode(req.headers.authorization).id,
        reqBody: req.body
    }
    bookController.addReview(data).then(resultFromController => res.send(resultFromController));
});	

//<<-------------EDIT REVIEW ---------------->>\\
router.post("/:bookId/editReview", auth.verify, (req, res) => {
    const data = {
        reqParams: req.params,
        userId: auth.decode(req.headers.authorization).id,
        reqBody: req.body
    }
    bookController.editReview(data).then(resultFromController => res.send(resultFromController));
});	

//<<-------------DELETE REVIEW ---------------->>\\
router.post("/:bookId/deleteReview", auth.verify, (req, res) => {
    const data = {
        reqParams: req.params,
        userId: auth.decode(req.headers.authorization).id,
        reqBody: req.body
    }
    bookController.deleteReview(data).then(resultFromController => res.send(resultFromController));
});	

//<<-------------ON SALE ALL ITEMS (ADMIN)---------------->>\\
router.post("/putOnSale",auth.verify, (req,res) => {
	const data = {
		reqBody: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	bookController.onSale(data).then(resultFromController => res.send(resultFromController));
});

//<<-------------ON SALE ITEM BY GENRE (ADMIN)---------------->>\\
router.post("/putOnSaleByGenre",auth.verify, (req,res) => {
	const data = {
		reqBody: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	bookController.onSaleByGenre(data).then(resultFromController => res.send(resultFromController));
});

//<<-------------ADD TO WISHLIST---------------->>\\
router.post("/:bookId/addToWishlist", auth.verify, (req, res) => {
    const data = {
        reqParams: req.params,
        userId: auth.decode(req.headers.authorization).id,
        reqBody: req.body
    }
    bookController.addToWishlist(data).then(resultFromController => res.send(resultFromController));
});	

//<<-------------SEARCH BOOKS---------------->>\\
router.post("/query", (req, res) => {
    bookController.searchBooks(req.body).then(resultFromController => res.send(resultFromController));
});	

module.exports = router;