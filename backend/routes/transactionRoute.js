const express = require("express");
const router = express.Router();
const auth = require("../auth");
const transactionController = require("../controllers/transactionController");

//<<-------------EDIT STATUS---------------->>\\
router.post("/:transactionId/edit", auth.verify, (req, res) => {
	const data = {
	isAdmin: auth.decode(req.headers.authorization).isAdmin,
	reqBody: req.body,
	transactionId: req.params.transactionId
}
transactionController.editTransaction(data).then(resultFromController => res.send(resultFromController));
});

//<<-------------RETRIEVE PENDING STATUS---------------->>\\
router.post("/pending", auth.verify, (req, res) => {
	const data = {
	isAdmin: auth.decode(req.headers.authorization).isAdmin
}
transactionController.getPendingTransactions(data).then(resultFromController => res.send(resultFromController));
});


router.patch("/pendingToShip", auth.verify, (req, res) => {
	const data = {
	isAdmin: auth.decode(req.headers.authorization).isAdmin,
	transactionId: req.body.transactionId
}
transactionController.shippedOutButton(data).then(resultFromController => res.send(resultFromController));
});

router.patch("/shipToDelivered", auth.verify, (req, res) => {
	const data = {
	isAdmin: auth.decode(req.headers.authorization).isAdmin,
	transactionId: req.body.transactionId
}
transactionController.deliverButton(data).then(resultFromController => res.send(resultFromController));
});


//<<-------------RETRIEVE SHIPPED OUT STATUS---------------->>\\
router.post("/shippedOut", auth.verify, (req, res) => {
	const data = {
	isAdmin: auth.decode(req.headers.authorization).isAdmin
}
transactionController.getShippedOutTransactions(data).then(resultFromController => res.send(resultFromController));
});

//<<-------------RETRIEVE DELIVERED STATUS---------------->>\\
router.post("/delivered", auth.verify, (req, res) => {
	const data = {
	isAdmin: auth.decode(req.headers.authorization).isAdmin
}
transactionController.getDeliveredTransactions(data).then(resultFromController => res.send(resultFromController));
});


router.post("/cancelled", auth.verify, (req, res) => {
	const data = {
	isAdmin: auth.decode(req.headers.authorization).isAdmin
}
transactionController.getCancelledTransactions(data).then(resultFromController => res.send(resultFromController));
});


//<<-------------RETRIEVE ALL TRANSACTIONS---------------->>\\
router.post("/", auth.verify, (req, res) => {
	const data = {
	isAdmin: auth.decode(req.headers.authorization).isAdmin
}
transactionController.getAllTransactions(data).then(resultFromController => res.send(resultFromController));
});


module.exports = router;
