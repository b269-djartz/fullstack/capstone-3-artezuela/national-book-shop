const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	userId:{
		type: String,
		required: [true, "UserId is required."]
	},
	books:{
		bookId:{
			type: String,
			required: [true, "BookId is required."]
		},
		price:{
			type: Number,
			required: [true, "Price is required."]
		},
		title:{
			type: String,
			required: [true, "BookId is required."]
		},
		quantity:{
			type: Number,
			default: 1
		},
		stock: {
			type: Number,
			required: [true, "Stock is required."]
		}
	},
	subtotal:{
		type: Number,
		required: [true, "Total is required."]
	},
	purchasedOn:{
		type: Date,
		default: new Date()
	},
	status: {
		type: String,
		default: "Added to cart"
	}
}
);

module.exports = mongoose.model("Order", orderSchema)