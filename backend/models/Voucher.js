const mongoose = require("mongoose");

const voucherSchema = new mongoose.Schema({
name: {
  type: String,
  required: [true, "Name is required"]
},
voucherName: {
  type: String,
  required: [true, "voucherName is required"]
},
discount: {
  type: Number,
  required: [true, "Discount is required"]
},
numberOfVoucherUse: {
  type: Number,
  default: 0
},
commision: {
  type: Number,
  dafault: 0
}
});

module.exports = mongoose.model("Voucher", voucherSchema);

