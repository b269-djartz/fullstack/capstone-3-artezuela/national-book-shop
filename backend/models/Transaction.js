const mongoose = require('mongoose');

const TransactionSchema = new mongoose.Schema({
    userId: {
        type: String,
        required: true
    },
    totalAmount: {
        type: Number,
        required: true
    },
    subtotal: {
        type: Number,
        required: true
    },
    shippingFee: {
        type: Number,
        required: true
    },
    voucherDiscount: {
        type: Number,
        required: true
    },
    status: {
        type: String,
        required: true
    },
    books: [{
        bookId: {
            type: String,
            required: true  
        },
        title: {
            type: String,
            required: true
        },
        quantity: {
            type: Number,
            required: true
        },
        price: {
            type: Number,
            required: true
        }
        
    }],
    paymentMethod: {
        type: String,
        enum: ['cashOnDelivery', 'E-wallet'],
        required: true
    },
    createdOn: {
    type: Date,
    default: new Date()
  },
    shippingAddress:{
        province:{
            type: String,
            required: [true, "Province is required."]
        },
        municipality:{
            type: String,
            required: [true, "Price is required."]
        },
        region:{
            type: String,
            required: [true, "Region is required."]
        },
        barangay:{
            type: String,
            default: [true, "Barangay is required."]
        },
        completeAddress: {
            type: String,
            required: [true, "Complete Address is required."]
        }
    }
});

module.exports = mongoose.model('Transaction', TransactionSchema);

