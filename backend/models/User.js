const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
username: {
  type: String,
  required: [true, "Fullname is required"]
},
name: {
  type: String,
  required: [true, "Fullname is required"]
},
location: {
  completeAddress: {
    type: String,
    required: [true, "Complete address is required"]
  },
  barangay: {
    type: String,
    required: [true, "barangay is required"]
  },
  municipality: {
    type: String,
    required: [true, "municipality is required"]
  },
  province: {
    type: String,
    required: [true, "province is required"]
  },
  region: {
    type: String,
    required: [true, "region is required"]
  },
},
email: {
  type: String,
  required: [true, "Email is required"]
},
password: {
  type: String,
  required: [true, "Password is required"]
},
isAdmin: {
  type: Boolean,
  default: false
},
wishlist: {
  type: [String],
  default: []
},
booksPurchased: {
  type: [String],
  default: []
},
reviews: {
  type: [String],
  default: []
  },

shippingAddresses: [{
  completeAddress: {
    type: String,
    required: [true, "Complete address is required"]
  },
  barangay: {
    type: String,
    required: [true, "barangay is required"]
  },
  municipality: {
    type: String,
    required: [true, "municipality is required"]
  },
  province: {
    type: String,
    required: [true, "province is required"]
  },
  region: {
    type: String,
    required: [true, "region is required"]
  },
  isActive: {
    type: Boolean,
    default : false
  }
}],
createdOn: {
  type: Date,
  default: new Date()
}
  
});

module.exports = mongoose.model("User", userSchema);
