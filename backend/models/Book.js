const mongoose = require("mongoose");

const bookSchema = new mongoose.Schema({
  title: {
    type: String,
    required: [true, "Title is required"]
  },
  genre: {
    type: String,
    required: [true, "Genre is required"]
  },
  author: {
    type: String,
    required: [true, "Author is required"]
  },
  description: {
    type: String,
    required: [true, "Description is required"]
  },
  price: {
    type: Number,
    required: [true, "Price is required"]
  },
  starRating: {
    type: Number,
    default: 0
  },
  ratings: {
    type: Number,
    default: 0
  },
  stock: {
    type: Number,
    required: [true, "Stock is required"]
  },
  sold: {
    type: Number,
    default: 0
  },
  createdOn: {
    type: Date,
    default: new Date()
  },
  reviews: [
    {
      username: {
        type: String,
        required: [true, "Username is required"]
      },
      name: {
        type: String,
        required: [true, "Name is required"]
      },
      star: {
        type: Number,
        enum: [1, 2, 3, 4, 5],
        required: [true, "Star rating is required"]
      },
      comment: {
        type: String,
        required: false
      }
    }
  ],
  tags: {
    type: [String],
    required: false
  }
});

module.exports = mongoose.model("Book", bookSchema);
