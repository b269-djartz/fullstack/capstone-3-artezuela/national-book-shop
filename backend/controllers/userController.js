const User = require("../models/User");
const Book = require("../models/Book");
const Transaction = require("../models/Transaction");
const Voucher = require("../models/Voucher");
const bcrypt = require("bcrypt");
const auth = require("../auth");


//<<-------------REGISTER---------------->>\\
module.exports.registerUser = async (reqBody) => {
  const existingUserEmail = await User.findOne({ email: reqBody.email });
  if (existingUserEmail) {
    return { "emailAlreadyExists": true };
  };

  const existingUserUsername = await User.findOne({ username: reqBody.username });
  if (existingUserUsername) {
    return { "usernameAlreadyExists": true };
  };

  const newUser = new User({
    name: reqBody.name,
    username: reqBody.username,
    location: {
      completeAddress: reqBody.completeAddress,
      barangay: reqBody.barangay,
      municipality: reqBody.municipality,
      province: reqBody.province,
      region: reqBody.region,
    },
    email: reqBody.email,
    password: bcrypt.hashSync(reqBody.password, 10),
    shippingAddresses: [{
      completeAddress: reqBody.completeAddress,
      barangay: reqBody.barangay,
      municipality: reqBody.municipality,
      province: reqBody.province,
      region: reqBody.region,
    }]
  });

  return newUser.save().then((user, error) => {
    if (error) {
      return { "inputError": true };
    } else {
      return true;
    };
  });
};



module.exports.changePassword = async (data) => {
  try {
    // Fetch the user's existing password from the database
    const user = await User.findById(data.userId);
    const currentPassword = user.password;

    // Validate the new password and confirm password
    const { password, confirmPassword } = data.reqBody;
    if (password !== confirmPassword) {
      return { "passwordMatches": false, "message": "Passwords do not match" };
    }
    const passwordMatches = await bcrypt.compare(password, currentPassword);
    if (passwordMatches) {
      // The new password is the same as the existing password
      return { "passwordMatches": true, "message": "New password cannot be same as the existing password" };
    }

    // Update the password
    const hashedPassword = await bcrypt.hash(password, 10);
    const updatePassword = {
      password: hashedPassword
    };
    const updatedUser = await User.findByIdAndUpdate(data.userId, updatePassword, { new: true });
    return true;
  } catch (error) {
    console.error(error);
    return false;
  }
};

module.exports.checkOldPassword = async (data) => {

    // Fetch the user's existing password from the database
    const user = await User.findById(data.userId);
    const currentPassword = user.password;

    // Compare the new password with the existing password
    const newPassword = data.reqBody.password;
    const passwordMatches = await bcrypt.compare(newPassword, currentPassword);
    if (passwordMatches) {
      // The new password is the same as the existing password
      return {"passwordMatches":true};
    } else {
    	return false
    }


};



//<<-------------USER AUTHENTICATION---------------->>\\
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result =>{
		if (result == null){
			return {emailNotFound : true }
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if (isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			} else {
				return {passwordIncorrect: true};
			};
		};
	});
};

//<<-------------RETRIEVE USER DETAILS---------------->>\\
module.exports.getProfile = (data) => {
	return User.findById(data.userId).then(result => {
		result.password = "";
		return result;
	});
};
//<<-------------RETRIEVE ALL USER DETAILS---------------->>\\

module.exports.getAllUsers = (data) => {
	if(data.isAdmin) {
		return User.find().then(result => {
		return result;
	});
	};
	const message = Promise.resolve('User must be ADMIN to access this!');
	return message.then((value) => {
		if(value){
			return {"userNotAdmin": true}
		}
	});
	
};

//<<-------------RETRIEVE Specific USER ---------------->>\\

module.exports.userView = (data) => {
	if(data.isAdmin) {
		return User.findById(data.reqParams.userId).then((result,error) => {
		if(error) {
			return false
		} else {
			return result
		}
	});
	}
	const message = Promise.resolve('User must be ADMIN to access this!');
	return message.then((value) => {
		if(value){
			return {"userNotAdmin": true}
		}
	});
	
};



//<<-------------SET USER AS ADMIN---------------->>\\
module.exports.setUserAsAdmin = (data) => {
	if(data.isAdmin) {
		const updateUserAsAdmin = {
		isAdmin: true
	};
	return User.findByIdAndUpdate(data.reqParams.userId, updateUserAsAdmin).then((course, error) => {
		if (error) {
			return false;
		} else {
			return true;
		};
	});
	}
	const message = Promise.resolve('User must be ADMIN to access this!');
	return message.then((value) => {
		if(value){
			return {"userNotAdmin": true}
		}
	});
	
};

//<<-------------RETRIEVE USER TRANSACTION---------------->>\\
module.exports.getUserTransactions = async (data) => {
	const userTransactions = await Transaction.find({userId: data.userId});
	if(userTransactions.length === 0) {
		return {"noTransactions": true };
	} else {
		return  userTransactions;
	}
};

//<<-------------RETRIEVE USER PENDING TRANSACTION---------------->>\\
module.exports.getUserPendingTransactions = async (data) => {
	const pendingTransactions = await Transaction.find({userId: data.userId,status: "Pending"});
	if(pendingTransactions.length === 0) {
		return {"noTransactions": true };
	} else {
		return pendingTransactions;
	}
};

//<<-------------RETRIEVE USER SHIPPED OUT TRANSACTION---------------->>\\
module.exports.getUserShippedOutTransactions = async (data) => {
	const shippedOutTransactions = await Transaction.find({userId: data.userId,status: "Shipped Out"});
	if(shippedOutTransactions.length === 0) {
		return {"noTransactions": true };
	} else {
		return shippedOutTransactions;
	}
};

//<<-------------RETRIEVE USER DELIVERED TRANSACTION---------------->>\\
module.exports.getUserDeliveredTransactions = async (data) => {
	const deliveredTransactions = await Transaction.find({userId: data.userId,status: "Delivered"});
	if(deliveredTransactions.length === 0) {
		return {"noTransactions": true };
	} else {
		return deliveredTransactions;
	}
};

//<<-------------RETRIEVE USER CANCELLED TRANSACTION---------------->>\\
module.exports.getUserCancelledTransactions = async (data) => {
	const cancelledTransactions = await Transaction.find({userId: data.userId,status: "Cancelled"});
	if(cancelledTransactions.length === 0) {
		return {"noTransactions": true };
	} else {
		return cancelledTransactions;
	}
};

//<<-------------CANCEL USER TRANSACTION---------------->>\\
module.exports.cancelTransaction = async (reqBody) => {
  const transaction = await Transaction.findById(reqBody.transactionId);

  if (!transaction) {
    return {"noTransactions": true };
  }

  // if (transaction.status === "Shipped out" || transaction.status === "Delivered") {
  //   return {"cancelDisabled": true };
  // }

  if (transaction.status !== "Pending") {
    return {"cancelDisabled": true };
  }

  transaction.status = "Cancelled";
  await transaction.save();

  // update product quantities and amounts sold
  for (let i = 0; i < transaction.books.length; i++) {
    const book = await Book.findOne({ title: transaction.books[i].title });

    if (!book) {
      return `Book with title ${transaction.books[i].title} not found`;
    }

    const quantity = transaction.books[i].quantity;
    book.stock += quantity;
    book.sold -= quantity;
    await book.save();
  }

  return true;
};



module.exports.addVoucher = async (data) => {
	// Check if the user is an admin
	if (data.isAdmin) {
		// Create a new Book object using the Book model and the data provided
		const existingVoucher = await Voucher.findOne({ name: data.voucher.name, voucherName: data.voucher.voucherName  });
      if (existingVoucher) {
        return { message: "Voucher already exists!" };
      }

    const newVoucher = new Voucher({
			name: data.voucher.name,
			voucherName: data.voucher.voucherName,
			discount: data.voucher.discount
		});
		await newVoucher.save();
		return true
	};

	// If the user is not an admin, return a Promise with a message
	const message = Promise.resolve('User must be ADMIN to access this!');
	return message.then((value) => {
		return {value};
	});
};

module.exports.viewVoucher = async (data) => {
	if(data.isAdmin) {
		const viewVoucher = await Voucher.find()
		if(viewVoucher) {
			return viewVoucher
		} else {
			false
		}
	}
}


module.exports.checkReview = async (data) => {
  const user = await User.findById(data.userId);
  const book = await Book.findById(data.reqBody.bookId);
  const bookReviews = book.reviews
  for(const bookReview of bookReviews) {
  	if(user.reviews.includes(data.reqBody.bookId) && bookReview.username === user.username) {
  		return true
  	}
  } return false
 
}

module.exports.checkBookPurchase= async (data) => {
  const user = await User.findById(data.userId);
  if(user.booksPurchased.includes(data.reqBody.bookId)) {
  	return true
  } else {
  	return false
  }
}


module.exports.checkUsersWishlist= async (data) => {
  const user = await User.findById(data.userId);
  if(user.wishlist.includes(data.reqBody.bookId)) {
  	return true
  } else {
  	return false
  }
}



module.exports.getUserLocation= async (data) => {
  const user = await User.findById(data.userId);
  if(user) {
  	return {"region" : user.location.region,
  					"province": user.location.province,
  					"municipality": user.location.municipality,
  					"barangay": user.location.barangay,
  					"completeAddress": user.location.completeAddress
  						}
  } else {
  	return false
  }
}

module.exports.addShippingAddress= async (data) => {
  const user = await User.findById(data.userId);
  const shippingAddresses = user.shippingAddresses
  let newAddress = {region : data.reqBody.region,
  					province: data.reqBody.province,
  					municipality:data.reqBody.municipality,
  					barangay: data.reqBody.barangay,
  					completeAddress: data.reqBody.completeAddress
  						}
  shippingAddresses.push(newAddress);
  user.shippingAddresses = shippingAddresses;
  await user.save();
  return true
}


module.exports.getUserShippingAddress= async (data) => {
  const user = await User.findById(data.userId);
  const shippingAddresses = user.shippingAddresses
  return shippingAddresses
}


module.exports.getUserShippingAddress2= async (data) => {
  const user = await User.findById(data.userId);
  const shippingAddresses = user.shippingAddresses;
  const activeShippingAddress = shippingAddresses.find(address => address.isActive === true);
  return activeShippingAddress;
}


module.exports.chooseShippingAddress = async (data) => {
  const user = await User.findById(data.userId);

  const addresses = user.shippingAddresses;

  for (const address of addresses) {
    if (address._id.toString() === data.reqBody.shippingAddressId.toString()) {
      address.isActive = true;
    } else {
      address.isActive = false;
    }
  }

  user.shippingAddresses = addresses;
  await user.save();

  return true;
};










/*module.exports.getOrders = (reqParams) => {
	return User.findById(reqParams.userId, 'orders').then(result => {
		return result;
	});
};

module.exports.getAllOrders = (data) => {
	if(data.isAdmin) {
	return User.find(data.reqOrder,"orders").then(result => {
		return result;
	});	
	}
	
};*/


/*module.exports.checkout = async(data) => {
	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.orders.push({bookId : data.bookId});
		return user.save().then((user,error) => {
			if(error) {
				return false;
			} else {
				return true;
			};
		});
	});
	// Add user ID in the enrollees array of the course
	let isBookUpdated = await Book.findById(data.bookId).then(book => {
		book.buyers.push({userId: data.userId});
		return book.save().then((book,error) => {
			if(error) {
				return false;
			} else {
				return true;
			};
		});
	});

	if(isUserUpdated && isBookUpdated) {
		return true;
	} else {
		return false;
	}




};*/
