const Order = require("../models/Order")
const Book = require("../models/Book")
const User = require("../models/User")
const Transaction = require("../models/Transaction")
const Voucher = require("../models/Voucher")
const auth = require("../auth");


//<<-------------ADD TO CART---------------->>\\
module.exports.addToCart = async (data) => {
  if (data.isAdmin) {
    return { adminRestricted: true };
  }

  const book = await Book.findById(data.reqBody.bookId);
  if (!book) {
    return `Book with ID ${data.reqBody.bookId} not found`;
  }

  const requestedQuantity = 1;
  if (book.stock < requestedQuantity) {
    return `The quantity you are requesting is more than the amount of stock that is available`;
  }

  // Check if the user already has an order with the same book ID
  let existingOrder = await Order.findOne({
    userId: data.userId,
    "books.bookId": data.reqBody.bookId,
    status: "Added to cart",
  });

  if (existingOrder) {
    // If an order with the same book ID already exists, update the quantity
    const existingQuantity = existingOrder.books.quantity;
    const newQuantity = existingQuantity + requestedQuantity;
    if (book.stock < newQuantity) {
      return { "requestExceedsStock": true };
    } 

    existingOrder.books.quantity = newQuantity;
    existingOrder.subtotal = book.price * newQuantity;
    await existingOrder.save();
  } else {
    // If no order with the same book ID exists, create a new one
    const subtotal = book.price * requestedQuantity;
    const newOrder = new Order({
      userId: data.userId,
      books: {
        bookId: data.reqBody.bookId,
        title: book.title,
        price: book.price,
        quantity: requestedQuantity,
        stock: book.stock 
      },
      subtotal: subtotal,
    });
    await newOrder.save();
  }

  return true;
};

//<<-------------VIEW MY CART--------------->>\\
module.exports.myCart = (userData) => {
	return Order.find({status: "Added to cart", userId: userData}).then((result) => { 
		return result
	});
}


//<<-------------VIEW MY CART--------------->>\\
module.exports.changeOrder = (data) => {
  const newBooks = data.reqBody.books
  return Order.findOneAndUpdate({status: "Added to cart", userId: data.userId}).then((result) => { 
    return result
  });
}



//<<-------------CHANGE QUANTITY--------------->>\\
module.exports.changeQuantity = async (reqParams, reqBody) => {
  // Check if the requested quantity is 0
  if (reqBody.quantity === 0) {
    // Delete the order if the quantity is 0
    const deletedOrder = await Order.findByIdAndDelete(reqParams.orderId);
    if (deletedOrder) {
      // Return true if the order was deleted successfully
      return "Order deleted";
    } else {
      // Return false if the order could not be deleted
      return "Order could not be deleted";
    }
  } else {
    // Find the order that needs to be updated
    const order = await Order.findById(reqParams.orderId);
    if (!order) {
      // Return false if the order cannot be found
      return "Order cant be found";
    }

    // Find the book associated with the order
    const book = await Book.findById(order.books.bookId);
    if (!book) {
      // Return false if the book cannot be found
      return "Book cannot be found";
    }

    // Check if the requested quantity exceeds the book's stock
    if (reqBody.quantity > book.stock) {
      // Return false if the requested quantity exceeds the book's stock
      return { "requestExceedsStock": true };
    }

    // Calculate the new subtotal based on the updated quantity and the price of the book
    const newSubtotal = reqBody.quantity * book.price;

    // Update the order with the new quantity and subtotal
    const updatedOrder = await Order.findOneAndUpdate(
      { _id: reqParams.orderId },
      { $set: { "books.quantity": reqBody.quantity, subtotal: newSubtotal } },
      { new: true }
    );
    if (updatedOrder) {
      // Return true if the order was updated successfully
      return true;
    } else {
      // Return false if the order could not be updated
      return "Order could not be updated";
    }
  }
};




//<<-------------REMOVE PRODUCT FROM CART--------------->>\\
module.exports.removeProduct = (reqParams) => {
	return Order.findByIdAndDelete(reqParams.orderId).then((course, error) => {
		if (error) {
			return false;
		} else {
			return true;
		};
	});
	
};

//<<-------------TOTAL AMOUNT IN CART--------------->>\\
module.exports.totalAmount = async (data) => {
  const user = await User.findById(data.userData);
  if (!user) {
    return "User not found";
  }

  const location = data.reqBody.province.toLowerCase();
  const region = user.location.region
  const province = data.reqBody.province
  const municipality = user.location.municipality
  const barangay = user.location.barangay
  const completeAddress = user.location.completeAddress

  if(location==="batanes" || location==="sulu" || location==="tawi-tawi"){
    return "Shipping to your location not available."
  }

  let shippingFee = 0;
  if (location === "abra") {
    shippingFee = 145;
  } else if (location === "apayao") {
    shippingFee = 184;
  } else if (location === "benguet") {
    shippingFee = 105;
  } else if (location === "ifugao") {
    shippingFee = 117;
  } else if (location === "kalinga") {
    shippingFee = 157;
  } else if (location === "mountain province") {
    shippingFee = 135;
  } else if (location === "ilocos norte") {
    shippingFee = 168;
  } else if (location === "ilocos sur") {
    shippingFee = 122;
  } else if (location === "la union") {
    shippingFee = 89;
  } else if (location === "pangasinan") {
    shippingFee = 71;
  } else if (location === "cagayan") {
    shippingFee = 195;
  } else if (location === "isabela") {
    shippingFee = 135;
  } else if (location === "nueva vizcaya") {
    shippingFee = 93;
  } else if (location === "quirino") {
    shippingFee = 124;
  } else if (location === "aurora") {
    shippingFee = 170;
  } else if (location === "bataan") {
    shippingFee = 170;
  } else if (location === "bulacan") {
    shippingFee = 170;
  } else if (location === "nueva ecija") {
    shippingFee = 170;
  } else if (location === "pampanga") {
    shippingFee = 170;
  } else if (location === "tarlac") {
    shippingFee = 170;
  } else if (location === "zambales") {
    shippingFee = 170;
  } else if (location === "batangas") {
    shippingFee = 170;
  } else if (location === "cavite") {
    shippingFee = 170;
  } else if (location === "laguna") {
    shippingFee = 170;
  } else if (location === "quezon") {
    shippingFee = 170;
  } else if (location === "rizal") {
    shippingFee = 170;
  } else if (location === "occidental mindoro") {
    shippingFee = 170;
  } else if (location === "oriental mindoro") {
    shippingFee = 170;
  } else if (location === "palawan") {
    shippingFee = 170;
  } else if (location === "romblon") {
    shippingFee = 170;
  } else if (location === "albay") {
    shippingFee = 170;
  } else if (location === "camarines norte") {
    shippingFee = 170;
  } else if (location === "camarines sur") {
    shippingFee = 170;
  } else if (location === "catanduanes") {
    shippingFee = 170;
  } else if (location === "masbate") {
    shippingFee = 170;
  } else if (location === "sorsogon") {
    shippingFee = 170;
  } else if (location === "aklan") {
    shippingFee = 170;
  } else if (location === "antique") {
    shippingFee = 170;
  } else if (location === "capiz") {
    shippingFee = 170;
  } else if (location === "guimaras") {
    shippingFee = 170;
  } else if (location === "iloilo") {
    shippingFee = 170;
  } else if (location === "negros occidental") {
    shippingFee = 170;
  } else if (location === "bohol") {
    shippingFee = 200;
  } else if (location === "cebu") {
    shippingFee = 170;
  } else if (location === "negros oriental") {
    shippingFee = 170;
  } else if (location === "siquijor") {
    shippingFee = 170;
  } else if (location === "biliran") {
    shippingFee = 250;
  } else if (location === "eastern samar") {
    shippingFee = 250;
  } else if (location === "leyte") {
    shippingFee = 250;
  } else if (location === "northern samar") {
    shippingFee = 250;
  } else if (location === "samar (western samar)") {
    shippingFee = 250;
  } else if (location === "southern leyte") {
    shippingFee = 250;
  } else if (location === "zamboanga del norte") {
    shippingFee = 170;
  }else if (location === "zamboanga del sur") {
    shippingFee = 170;
  }else if (location === "zamboanga sibugay") {
    shippingFee = 170;
  }else if (location === "bukidnon") {
    shippingFee = 170;
  }else if (location === "camiguin") {
    shippingFee = 170;
  }else if (location === "lanao del norte") {
    shippingFee = 170;
  }else if (location === "misamis occidental") {
    shippingFee = 170;
  }else if (location === "misamis oriental") {
    shippingFee = 170;
  }else if (location === "davao de oro") {
    shippingFee = 170;
  }else if (location === "davao del norte") {
    shippingFee = 170;
  }else if (location === "davao del sur") {
    shippingFee = 170;
  }else if (location === "davao occidental") {
    shippingFee = 170;
  }else if (location === "davao oriental") {
    shippingFee = 170;
  }else if (location === "cotabato") {
    shippingFee = 170;
  }else if (location === "sarangani") {
    shippingFee = 170;
  }else if (location === "south cotabato") {
    shippingFee = 170;
  }else if (location === "sultan kudarat") {
    shippingFee = 170;
  }else if (location === "agusan del norte") {
    shippingFee = 170;
  }else if (location === "agusan del sur") {
    shippingFee = 170;
  }else if (location === "dinagat islands") {
    shippingFee = 170;
  }else if (location === "surigao del norte") {
    shippingFee = 170;
  }else if (location === "surigao del sur") {
    shippingFee = 170;
  }else if (location === "basilan") {
    shippingFee = 170;
  }else if (location === "lanao del sur") {
    shippingFee = 170;
  }else if (location === "maguidanao del norte") {
    shippingFee = 170;
  }else if (location === "maguindanao del sur") {
    shippingFee = 170;
  }else if (location === "sulu") {
    shippingFee = 170;
  }else if (location === "tawi-tawi") {
    shippingFee = 170;
  } else {
    shippingFee = 170
  }

  const orders = await Order.aggregate([
    {
      $match: { userId: data.userData, status: "Added to cart" }
    },
    {
      $group: {
        _id: "$userId",
        subtotal: { $sum: "$subtotal" }
      }
    }
  ]);

  const orderQuantity = await Order.aggregate([
    {
      $match: { userId: data.userData, status: "Added to cart" }
    },
    {
      $unwind: "$books"
    },
    {
      $group: {
        _id: "$userId",
        totalQuantity: { $sum: "$books.quantity" }
      }
    }
  ]);


  let voucherDiscount = 0;
  if (data.reqBody.voucher) {
    const voucher = await Voucher.findOne({ voucherName: data.reqBody.voucher });
    if (voucher) {
      voucherDiscount = voucher.discount;
    } else {
      voucherDiscount = 0
    }
  }

  if (orders.length > 0) {

    const subtotal = orders[0].subtotal;
    const totalQuantity = orderQuantity[0].totalQuantity;
    let shippingFeeIncrease = 0;
    if (totalQuantity > 5 && totalQuantity <= 10) {
      shippingFeeIncrease = 150;
    } else if (totalQuantity > 10 && totalQuantity <= 20) {
      shippingFeeIncrease = 350;
    }
    shippingFee += shippingFeeIncrease;
    const total = subtotal + shippingFee - voucherDiscount;
    return { "subtotal": subtotal,
              "shippingFee": shippingFee,
              "voucherDiscount": voucherDiscount,
              "totalAmount" : total,
              "region" : region,
              "municipality": municipality,
              "province": province,
              "barangay" : barangay,
              "completeAddress": completeAddress

              }


    /*`Your total amount will be:
              
            subtotal: ${subtotal}
                             +
         shippingFee: ${shippingFee}
                             -
     voucherDiscount: ${voucherDiscount}

     ------------------------------------------
         totalAmount: ${total}
            `;*/
  } else {
    return `Add to cart first `;
  }
};


//<<-------------CHECKOUT--------------->>\\
module.exports.checkout = async (data) => {

  // Check if the user has any orders to checkout
  const orders = await Order.find({userId:data.userData, status: "Added to cart"});
  if(orders.length === 0) {
      return "No orders to checkout";
  }

  // Calculate the total amount and subtotal by iterating over each order and adding up the subtotals of all orders
  let totalAmount = 0;
  let subtotal = 0;
  for(const order of orders) {
      subtotal += order.subtotal;
      totalAmount += order.subtotal;
  }

  // Add shipping fee and voucher discount to the total amount, if applicable
  const user = await User.findById(data.userData);
  if (!user) {
      return "User not found";
  }

  const location = (data.reqBody.province).toLowerCase();
  
  const region = `Region ${data.reqBody.region}`
  const province = data.reqBody.province
  const municipality = data.reqBody.municipality
  const barangay = data.reqBody.barangay
  const completeAddress = data.reqBody.completeAddress


    if(location==="batanes" || location==="sulu" || location==="tawi-tawi"){
      return "Shipping to your location not available."
    }

    let shippingFee = 0;
      if (location === "abra") {
        shippingFee = 145;
      } else if (location === "apayao") {
        shippingFee = 184;
      } else if (location === "benguet") {
        shippingFee = 105;
      } else if (location === "ifugao") {
        shippingFee = 117;
      } else if (location === "kalinga") {
        shippingFee = 157;
      } else if (location === "mountain province") {
        shippingFee = 135;
      } else if (location === "ilocos norte") {
        shippingFee = 168;
      } else if (location === "ilocos sur") {
        shippingFee = 122;
      } else if (location === "la union") {
        shippingFee = 89;
      } else if (location === "pangasinan") {
        shippingFee = 71;
      } else if (location === "cagayan") {
        shippingFee = 195;
      } else if (location === "isabela") {
        shippingFee = 135;
      } else if (location === "nueva vizcaya") {
        shippingFee = 93;
      } else if (location === "quirino") {
        shippingFee = 124;
      } else if (location === "aurora") {
        shippingFee = 170;
      } else if (location === "bataan") {
        shippingFee = 170;
      } else if (location === "bulacan") {
        shippingFee = 170;
      } else if (location === "nueva ecija") {
        shippingFee = 170;
      } else if (location === "pampanga") {
        shippingFee = 170;
      } else if (location === "tarlac") {
        shippingFee = 170;
      } else if (location === "zambales") {
        shippingFee = 170;
      } else if (location === "batangas") {
        shippingFee = 170;
      } else if (location === "cavite") {
        shippingFee = 170;
      } else if (location === "laguna") {
        shippingFee = 170;
      } else if (location === "quezon") {
        shippingFee = 170;
      } else if (location === "rizal") {
        shippingFee = 170;
      } else if (location === "occidental mindoro") {
        shippingFee = 170;
      } else if (location === "oriental mindoro") {
        shippingFee = 170;
      } else if (location === "palawan") {
        shippingFee = 170;
      } else if (location === "romblon") {
        shippingFee = 170;
      } else if (location === "albay") {
        shippingFee = 170;
      } else if (location === "camarines norte") {
        shippingFee = 170;
      } else if (location === "camarines sur") {
        shippingFee = 170;
      } else if (location === "catanduanes") {
        shippingFee = 170;
      } else if (location === "masbate") {
        shippingFee = 170;
      } else if (location === "sorsogon") {
        shippingFee = 170;
      } else if (location === "aklan") {
        shippingFee = 170;
      } else if (location === "antique") {
        shippingFee = 170;
      } else if (location === "capiz") {
        shippingFee = 170;
      } else if (location === "guimaras") {
        shippingFee = 170;
      } else if (location === "iloilo") {
        shippingFee = 170;
      } else if (location === "negros occidental") {
        shippingFee = 170;
      } else if (location === "bohol") {
        shippingFee = 170;
      } else if (location === "cebu") {
        shippingFee = 170;
      } else if (location === "negros oriental") {
        shippingFee = 170;
      } else if (location === "siquijor") {
        shippingFee = 170;
      } else if (location === "biliran") {
        shippingFee = 170;
      } else if (location === "eastern samar") {
        shippingFee = 170;
      } else if (location === "leyte") {
        shippingFee = 170;
      } else if (location === "northern samar") {
        shippingFee = 170;
      } else if (location === "samar (western samar)") {
        shippingFee = 170;
      } else if (location === "southern leyte") {
        shippingFee = 170;
      } else if (location === "zamboanga del norte") {
        shippingFee = 170;
      }else if (location === "zamboanga del sur") {
        shippingFee = 170;
      }else if (location === "zamboanga sibugay") {
        shippingFee = 170;
      }else if (location === "bukidnon") {
        shippingFee = 170;
      }else if (location === "camiguin") {
        shippingFee = 170;
      }else if (location === "lanao del norte") {
        shippingFee = 170;
      }else if (location === "misamis occidental") {
        shippingFee = 170;
      }else if (location === "misamis oriental") {
        shippingFee = 170;
      }else if (location === "davao de oro") {
        shippingFee = 170;
      }else if (location === "davao del norte") {
        shippingFee = 170;
      }else if (location === "davao del sur") {
        shippingFee = 170;
      }else if (location === "davao occidental") {
        shippingFee = 170;
      }else if (location === "davao oriental") {
        shippingFee = 170;
      }else if (location === "cotabato") {
        shippingFee = 170;
      }else if (location === "sarangani") {
        shippingFee = 170;
      }else if (location === "south cotabato") {
        shippingFee = 170;
      }else if (location === "sultan kudarat") {
        shippingFee = 170;
      }else if (location === "agusan del norte") {
        shippingFee = 170;
      }else if (location === "agusan del sur") {
        shippingFee = 170;
      }else if (location === "dinagat islands") {
        shippingFee = 170;
      }else if (location === "surigao del norte") {
        shippingFee = 170;
      }else if (location === "surigao del sur") {
        shippingFee = 170;
      }else if (location === "basilan") {
        shippingFee = 170;
      }else if (location === "lanao del sur") {
        shippingFee = 170;
      }else if (location === "maguidanao del norte") {
        shippingFee = 170;
      }else if (location === "maguindanao del sur") {
        shippingFee = 170;
      }else if (location === "sulu") {
        shippingFee = 170;
      }else if (location === "tawi-tawi") {
        shippingFee = 170;
      } else {
        shippingFee = 170
      }

  const orderQuantity = await Order.aggregate([
    {
      $match: { userId: data.userData, status: "Added to cart" }
    },
    {
      $unwind: "$books"
    },
    {
      $group: {
        _id: "$userId",
        totalQuantity: { $sum: "$books.quantity" }
      }
    }
  ]);

  const totalQuantity = orderQuantity[0].totalQuantity;
  let shippingFeeIncrease = 0;
  if (totalQuantity > 5 && totalQuantity <= 10) {
    shippingFeeIncrease = 150;
  } else if (totalQuantity > 10 && totalQuantity <= 20) {
    shippingFeeIncrease = 350;
  }
  shippingFee += shippingFeeIncrease;

  totalAmount += shippingFee;

  let voucherDiscount = 0;
  if (data.reqBody.voucher) {
    const voucher = await Voucher.findOne({ voucherName: data.reqBody.voucher });
    if (voucher) {
      voucherDiscount = voucher.discount;

      // Update voucher database
      voucher.numberOfVoucherUse += 1;
      voucher.commision = 50 * voucher.numberOfVoucherUse;
      await voucher.save();
    } else {
      voucherDiscount = 0
    }
  }
  
  totalAmount -= voucherDiscount;

  // Check payment method
  let paymentMethod = data.reqBody.paymentMethod;
  if (paymentMethod !== "cashOnDelivery" && paymentMethod !== "E-wallet") {
      return {"invalidPaymentMethod" : true};
  }

  if (paymentMethod === "E-wallet") {
      const paymentAmount = data.reqBody.paymentAmount;
      if (!paymentAmount || paymentAmount < totalAmount) {
          return  {"invalidPayment" : true};
      }
  } if (paymentMethod === "cashOnDelivery") {
    const paymentAmount = data.reqBody.paymentAmount;
  }

  // Iterate over each order and update the stock of the corresponding book
 // Iterate over each order and update the stock of the corresponding book
for (const order of orders) {
  const book = await Book.findById(order.books.bookId);
  if (!book) {
    return {"book1": true};
  }
  if (book.stock < order.books.quantity) {
    return `The stock of ${book.title} is less than the order quantity`;
  }
  book.stock -= order.books.quantity;
  book.sold += order.books.quantity; // increment sold field by orderQuantity
  await book.save();

  // Remove book from user's wishlist
  const index = user.wishlist.indexOf(book._id);
  if (index > -1) {
    user.wishlist.splice(index, 1);
    await user.save();
  }
}
  

  // Update the status and total amount of all orders to "Checked out"
  await Order.updateMany({userId: data.userData, status: "Added to cart"}, {status: "Checked out", totalAmount: totalAmount});

  // Create a new transaction record for the user
  const books = [];

  for (const order of orders) {
    const book = await Book.findById(order.books.bookId);
    
    if (!book) {
      return `Book with ID ${order.books.bookId} not found`;
    }
    
    books.push({
      bookId: order.books.bookId,
      title: book.title,
      quantity: order.books.quantity,
      price: order.books.price,
    });

    // Remove book from book's wishlist
    book.inWishlist = false;
    await book.save();
  }

  let shippingAddress = {
  region: "",
  province: "",
  municipality: "",
  barangay: "",
  completeAddress: ""
}

shippingAddress.region = region;
shippingAddress.province = province;
shippingAddress.municipality = municipality;
shippingAddress.barangay = barangay;
shippingAddress.completeAddress = completeAddress;

  const newTransaction = new Transaction({
      userId: data.userData,
      totalAmount: totalAmount,
      subtotal: subtotal,
      shippingFee: shippingFee,
      voucherDiscount: voucherDiscount,
      status: "Pending",
      books: books,
      paymentMethod: paymentMethod,
      shippingAddress: shippingAddress
  });
 

await newTransaction.save();




return { "allTrue": true,
              "region" : region,
              "municipality": municipality,
              "province": province,
              "barangay" : barangay,
              "completeAddress": completeAddress

              }

};




































// code for deleting the cart if quanity will be changed to 0
/*
module.exports.changeQuantity = async (reqParams, reqBody) => {
  if (reqBody.quantity === 0) {
    const deletedOrder = await Order.findByIdAndDelete(reqParams.orderId);
    if (deletedOrder) {
      return true;
    } else {
      return false;
    }
  } else {
    const order = await Order.findById(reqParams.orderId);
    if (!order) {
      return false;
    }

    const book = await Book.findById(order.books[0].bookId);
    if (!book) {
      return false;
    }

    if (reqBody.quantity > book.stock) {
      return false;
    }

    const updatedOrder = await Order.findByIdAndUpdate(
      reqParams.orderId,
      { $set: reqBody },
      { new: true }
    );
    if (updatedOrder) {
      return true;
    } else {
      return false;
    }
  }
};

*/


/*module.exports.checkout = async (data) => {
	
	// Check if the user has any orders to checkout
	const orders = await Order.find({userId:data.userData, status: "Added to cart"});
	if(orders.length === 0) {
		return "No orders to checkout";
	}
	
	// Iterate over each order and update the stock of the corresponding book
	for(const order of orders) {
		const book = await Book.findById(order.books[0].bookId);
		const orderQuantity = order.books[0].quantity;
		if(book.stock < orderQuantity) {
			return `The stock of ${book.name} is less than the order quantity`;
		}
		book.stock -= orderQuantity;
		await book.save();
	}
	
	// Update the status of all orders to "Checked out"
	await Order.updateMany({userId: data.userData, status: "Added to cart"}, {status: "Checked out"});
	
	return "Checkout successful";
};*/



//transaction
/*
module.exports.checkout = async (data) => {
	
	// Check if the user has any orders to checkout
	const orders = await Order.find({userId: data.userData, status: "Added to cart"});
	if(orders.length === 0) {
		return "No orders to checkout";
	}
	
	// Compute the total amount including shipping fee and voucher discount
	const totalAmount = await module.exports.totalAmount(data);
	
	// Iterate over each order and update the stock of the corresponding book
	for(const order of orders) {
		const book = await Book.findById(order.books[0].bookId);
		const orderQuantity = order.books[0].quantity;
		if(book.stock < orderQuantity) {
			return `The stock of ${book.name} is less than the order quantity`;
		}
		book.stock -= orderQuantity;
		await book.save();
	}
	
	// Update the status and total amount of all orders to "Checked out"
	await Order.updateMany({userId: data.userData, status: "Added to cart"}, {status: "Checked out", totalAmount: totalAmount});
	
	// Create a new transaction record for the user
	const newTransaction = new Transaction({
		userId: data.userData,
		totalAmount: totalAmount,
		status: "Success"
	});
	await newTransaction.save();
	
	return "Checkout successful";
};
*/