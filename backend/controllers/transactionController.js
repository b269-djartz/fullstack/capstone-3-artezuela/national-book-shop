const Transaction = require("../models/Transaction");
const bcrypt = require("bcrypt");
const User = require("../models/User");
const Book = require("../models/Book")
const auth = require("../auth");

//<<-------------EDIT TRANSACTIONS--------------->>\\
module.exports.editTransaction = async (data) => {
    if(data.isAdmin) {
    	const updatedStatus = {
    		status: data.reqBody.status
    	}
    	const transaction = await Transaction.findByIdAndUpdate(data.transactionId, updatedStatus, { new: true });
    	if (!transaction) {
    	  return "Transaction not found";
    	}
    	if(data.reqBody.status === "Delivered") {
    		const transaction = await Transaction.findByIdAndUpdate(data.transactionId);
    		const user = await User.findById(transaction.userId)
    		const books = transaction.books
    		for ( const book of books) {
    			user.booksPurchased.push(book.bookId)
    		}
    		user.save().then((user,error) => {
    			if(error) {
    				return error
    			}
    		})
    		
    	}
    	transaction.save()
    	return true;
    }
    const message = Promise.resolve('User must be ADMIN to access this!');
    return message.then((value) => {
    	return {value};
    });

    
};

//<<-------------ALL PENDING TRANSACTIONS--------------->>\\
module.exports.getPendingTransactions = async (data) => {
	if(data.isAdmin) {
		const pendingTransactions = await Transaction.find({status: "Pending"});
		if(pendingTransactions.length === 0) {
		return {"noTransactions":true};
		} else {
			return pendingTransactions;
		}
	}
	const message = Promise.resolve('User must be ADMIN to access this!');
	return message.then((value) => {
		return {value};
	});
	
};

module.exports.shippedOutButton = async (data) => {
	if(data.isAdmin) {
		const updatedStatusToShippedOut = {
    		status: "Shipped Out"
    	}
		const shippedTheTransactions = await Transaction.findByIdAndUpdate(data.transactionId, updatedStatusToShippedOut, { new: true });
		if(!shippedTheTransactions) {
			return {"noTransactions":true}
		}
		shippedTheTransactions.save()
    	return true;
		
	}
	const message = Promise.resolve('User must be ADMIN to access this!');
	return message.then((value) => {
		return {value};
	});
	
};


module.exports.deliverButton = async (data) => {
	if(data.isAdmin) {
		const updatedStatusDelivered = {
    		status: "Delivered"
    	}
		const deliveredTheTransactions = await Transaction.findByIdAndUpdate(data.transactionId, updatedStatusDelivered, { new: true });
		if(!deliveredTheTransactions) {
			return {"noTransactions":true}
		}
		deliveredTheTransactions.save()
		const transaction = await Transaction.findByIdAndUpdate(data.transactionId);
    		const user = await User.findById(transaction.userId)
    		const books = transaction.books
    		for ( const book of books) {
    			user.booksPurchased.push(book.bookId)
    		}
    		user.save().then((user,error) => {
    			if(error) {
    				return error
    			}
    		})
    	return true;
		
	}
	const message = Promise.resolve('User must be ADMIN to access this!');
	return message.then((value) => {
		return {value};
	});
	
};



//<<-------------ALL SHIPPED OUT TRANSACTIONS--------------->>\\
module.exports.getShippedOutTransactions = async (data) => {
	if(data.isAdmin) {
		const shippedOutTransactions = await Transaction.find({status: "Shipped Out"});
		if(shippedOutTransactions.length === 0) {
		return {"noTransactions":true};
		} else {
			return shippedOutTransactions;
		}
	}
	const message = Promise.resolve('User must be ADMIN to access this!');
	return message.then((value) => {
		return {value};
	});
	
};

//<<-------------ALL PENDING TRANSACTIONS--------------->>\\
module.exports.getDeliveredTransactions = async (data) => {
	if(data.isAdmin) {
		const deliveredTransactions = await Transaction.find({status: "Delivered"});
		if(deliveredTransactions.length === 0) {
		return {"noTransactions":true};
		} else {
			return deliveredTransactions;
		}
	}
	const message = Promise.resolve('User must be ADMIN to access this!');
	return message.then((value) => {
		return {value};
	});
	
};


module.exports.getCancelledTransactions = async (data) => {
	if(data.isAdmin) {
		const cancelledTransactions = await Transaction.find({status: "Cancelled"});
		if(cancelledTransactions.length === 0) {
		return {"noTransactions":true};
		} else {
			return cancelledTransactions;
		}
	}
	const message = Promise.resolve('User must be ADMIN to access this!');
	return message.then((value) => {
		return {value};
	});
	
};


//<<-------------ALL TRANSACTIONS--------------->>\\
module.exports.getAllTransactions = async (data) => {
	if(data.isAdmin) {
		const allTransactions = await Transaction.find();
		if(allTransactions.length === 0) {
		return false;
		} else {
			return allTransactions;
		}
	}
	const message = Promise.resolve('User must be ADMIN to access this!');
	return message.then((value) => {
		return {value};
	});
	
};